package com.example.todo_app

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.todo_list_item.view.*

class RecyclerAdapter(private val todoItemClickListener: (Todo) -> Unit) :
    RecyclerView.Adapter<RecyclerViewHolder>() {
    var todoList: List<Todo> = emptyList()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val view = layoutInflater.inflate(R.layout.todo_list_item, parent, false)

        return RecyclerViewHolder(view)
    }

    override fun getItemCount(): Int {
        return todoList.size
    }

    override fun onBindViewHolder(holder: RecyclerViewHolder, position: Int) {
        val todo = todoList[position]
        holder.itemView.text_todo_title.text = todo.title
        holder.itemView.setOnClickListener {
            todoItemClickListener.invoke(todo)
        }
    }
}
