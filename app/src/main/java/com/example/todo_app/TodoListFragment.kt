package com.example.todo_app

import android.app.AlertDialog
import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.snackbar.Snackbar
import com.google.gson.Gson
import kotlinx.android.synthetic.main.fragment_todo_list.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.Dispatchers.Main
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class TodoListFragment : Fragment() {

    private var isDeleteMode = false
    private val adapter = RecyclerAdapter(::onClickTodoListItem)

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        setHasOptionsMenu(true)
        (activity as? MainActivity)?.sharedViewModel?.todoEditResult?.observeEvent(this) {
            Snackbar.make(requireView(), it, Snackbar.LENGTH_LONG).show()
        }
        return inflater.inflate(R.layout.fragment_todo_list, container, false)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.menu_todo_list, menu)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val layoutManager = LinearLayoutManager(activity)
        recycler_todo.let {
            it.layoutManager = layoutManager
            it.adapter = adapter
            it.addItemDecoration(
                DividerItemDecoration(
                    activity,
                    DividerItemDecoration.VERTICAL
                )
            )
        }
        CoroutineScope(IO).launch {
            fetchTodoList()
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            // 作成ボタンを押したとき
            R.id.item_create -> {
                goToEditFragment()
            }
            // 削除ボタンを押したとき
            R.id.item_delete -> {
                isDeleteMode = !isDeleteMode
                val icon = if (isDeleteMode) {
                    R.drawable.ic_done_24px
                } else {
                    R.drawable.ic_delete_24px
                }
                item.setIcon(icon)
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private suspend fun fetchTodoList() {
        try {
            val response = TodoAPIClient().apiRequest.getTodos().execute()
            withContext(Main) {
                if (response.isSuccessful) {
                    adapter.todoList = response.body()?.todos!!
                } else {
                    val body =
                        Gson().fromJson(response.errorBody()!!.string(), CommonResponse::class.java)
                    showErrorDialog(body.errorMessage)
                }
            }
        } catch (e: Exception) {
            withContext(Main) {
                showErrorDialog(getString(R.string.unknown_error_message))
            }
        }
    }

    private suspend fun deleteTodo(id: Int) {
        try {
            val response = TodoAPIClient().apiRequest.deleteTodo(id).execute()
            if (response.isSuccessful) {
                fetchTodoList()
            } else {
                val body =
                    Gson().fromJson(response.errorBody()!!.string(), CommonResponse::class.java)
                withContext(Main) {
                    showErrorDialog(body.errorMessage)
                }
            }
        } catch (e: Exception) {
            withContext(Main) {
                showErrorDialog(getString(R.string.unknown_error_message))
            }
        }
    }

    private fun goToEditFragment(todo: Todo? = null) {
        val action = TodoListFragmentDirections.actionTodoEditFragment(todo)
        findNavController().navigate(action)
    }

    private fun showDeleteConfirmationDialog(todo: Todo) {
        AlertDialog.Builder(context)
            .setTitle(getString(R.string.delete_dialog_title, todo.title))
            .setMessage(R.string.delete_dialog_message)
            .setNegativeButton(android.R.string.cancel, null)
            .setPositiveButton(android.R.string.ok) { _, _ ->
                CoroutineScope(IO).launch {
                    deleteTodo(todo.id)
                }
            }
            .show()
    }

    private fun onClickTodoListItem(todo: Todo) {
        if (isDeleteMode) {
            showDeleteConfirmationDialog(todo)
        } else {
            goToEditFragment(todo)
        }
    }
}
