package com.example.todo_app

import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class TodoAPIClient {
    private val baseUrl = "https://todo-api-ymikami.herokuapp.com/"
    private val httpLogging = HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY)
    private val httpClientBuilder = OkHttpClient.Builder().addInterceptor(httpLogging)
    private val gson = GsonBuilder().serializeNulls().create()
    val apiRequest = Retrofit.Builder()
        .addConverterFactory(GsonConverterFactory.create(gson))
        .baseUrl(baseUrl)
        .client(httpClientBuilder.build())
        .build()
        .create(TodoInterface::class.java)
}
