package com.example.todo_app

import android.app.DatePickerDialog
import android.content.DialogInterface
import android.graphics.Color
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.google.gson.Gson
import kotlinx.android.synthetic.main.fragment_todo_edit.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.Dispatchers.Main
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.text.SimpleDateFormat
import java.util.*

class TodoEditFragment : Fragment() {
    private val args: TodoEditFragmentArgs by navArgs()
    private val displayFormat = SimpleDateFormat(FORMAT_DATE_APP, Locale.getDefault())
    private val sendFormat = SimpleDateFormat(FORMAT_DATE_SERVER, Locale.getDefault())

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_todo_edit, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val datePickerDialog = createDatePickerDialog()
        val todo = args.todo

        todo?.let { initTexts(it) }
        initTextCounter()
        initEditText()
        updateStateEditButton()
        initEditButton(todo?.id)
        text_date_content.setOnClickListener {
            datePickerDialog.show()
        }
    }

    private fun initTextCounter() {
        text_detail_counter.text = INITIAL_TEXT_COUNT.toString()
        text_title_counter.text = INITIAL_TEXT_COUNT.toString()
    }

    private fun updateStateEditButton() {
        val isTitleValid = edit_title.length() in 1..MAX_TITLE_LIMIT
        val isDetailValid = edit_detail.length() <= MAX_DETAIL_LIMIT
        button_edit.isEnabled = isTitleValid && isDetailValid
    }

    private fun createDatePickerDialog(): DatePickerDialog {
        val calender = Calendar.getInstance()
        val year = calender.get(Calendar.YEAR)
        val month = calender.get(Calendar.MONTH)
        val day = calender.get(Calendar.DAY_OF_MONTH)
        val datePickerDialog = DatePickerDialog(
            requireContext(), DatePickerDialog.OnDateSetListener { _, y, m, d ->
                text_date_content.text = ("$y/${m + 1}/$d")
            }, year, month, day
        )

        datePickerDialog.setButton(
            DialogInterface.BUTTON_NEUTRAL,
            getString(R.string.button_date_picker_delete)
        ) { _, _ ->
            text_date_content.text = ""
        }

        return datePickerDialog
    }

    private fun initEditText() {
        edit_title.addTextChangedListener(object : CustomTextWatcher {
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                updateStateEditButton()
                text_title_counter.text = p0?.length.toString()
                if (edit_title.length() > MAX_TITLE_LIMIT) {
                    text_title_counter.setTextColor(Color.RED)
                } else {
                    text_title_counter.setTextColor(Color.GRAY)
                }
            }
        })

        edit_detail.addTextChangedListener(object : CustomTextWatcher {
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                updateStateEditButton()
                text_detail_counter.text = p0?.length.toString()
                if (edit_detail.length() > MAX_DETAIL_LIMIT) {
                    text_detail_counter.setTextColor(Color.RED)
                } else {
                    text_detail_counter.setTextColor(Color.GRAY)
                }
            }
        })
    }

    private suspend fun registerTodo() {
        try {
            val response = TodoAPIClient().apiRequest.createTodo(createTodoParameter()).execute()
            withContext(Main) {
                if (response.isSuccessful) {
                    onSuccess(getString(R.string.registration_todo_message))
                } else {
                    val body =
                        Gson().fromJson(response.errorBody()!!.string(), CommonResponse::class.java)
                    showErrorDialog(body.errorMessage)
                }
            }
        } catch (e: Exception) {
            withContext(Main) {
                showErrorDialog(getString(R.string.unknown_error_message))
            }
        }
    }

    private suspend fun updateTodo(id: Int) {
        try {
            val response =
                TodoAPIClient().apiRequest.updateTodo(id, createTodoParameter()).execute()
            withContext(Main) {
                if (response.isSuccessful) {
                    onSuccess(getString(R.string.update_todo_message))
                } else {
                    val body =
                        Gson().fromJson(response.errorBody()!!.string(), CommonResponse::class.java)
                    showErrorDialog(body.errorMessage)
                }
            }
        } catch (e: Exception) {
            withContext(Main) {
                showErrorDialog(getString(R.string.unknown_error_message))
            }
        }
    }

    private fun createTodoParameter(): TodoRequestBody {
        val title = edit_title.text.toString()
        val detail = edit_detail.text.toString()
        val date =
            if (text_date_content.text.isBlank()) {
                ""
            } else {
                val parseDate =
                    displayFormat.parse(text_date_content.text.toString())
                sendFormat.format(parseDate)
            }

        return TodoRequestBody(title, detail, date)
    }

    private fun onSuccess(message: String) {
        (activity as? MainActivity)?.sharedViewModel?.todoEditResult?.value =
            Event(message)
        findNavController().navigateUp()
    }

    private fun initTexts(todo: Todo) {
        edit_title.setText(todo.title)
        todo.detail?.let { edit_detail.setText(it) }
        todo.date?.let { text_date_content.text = displayFormat.format(it) }
    }

    private fun initEditButton(id: Int?) {
        button_edit.text =
            getString(if (id == null) R.string.button_registration else R.string.button_update)
        button_edit.setOnClickListener {
            CoroutineScope(IO).launch {
                id?.let { updateTodo(id) } ?: registerTodo()
            }
        }
    }

    interface CustomTextWatcher : TextWatcher {
        override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
        override fun afterTextChanged(p0: Editable?) {}
    }

    companion object {
        private const val INITIAL_TEXT_COUNT = 0
        private const val MAX_TITLE_LIMIT = 100
        private const val MAX_DETAIL_LIMIT = 1000
        private const val FORMAT_DATE_APP = "yyyy/M/d"
        private const val FORMAT_DATE_SERVER = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
    }
}
