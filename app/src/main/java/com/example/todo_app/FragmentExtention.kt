package com.example.todo_app

import android.app.AlertDialog
import androidx.fragment.app.Fragment

fun Fragment.showErrorDialog(message: String) {
    AlertDialog.Builder(requireContext())
        .setTitle(R.string.title_error_dialog)
        .setMessage(message)
        .setPositiveButton(R.string.button_close_error_dialog, null)
        .show()
}
